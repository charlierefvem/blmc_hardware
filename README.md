# BLMC Hardware Repository

This repository contains the schematic and board files for the Brushless Motor Controller

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You will need Autodesk Eagle in order to view or edit the files in this repository

You may also want to install a git client compatible with BitBucket such as [Sourcetree](https://www.sourcetreeapp.com/).


## File List

### Eagle Files

There are two scripts in this repository that run separately. The first script uses the following files:

| Files                   | Type      | Description                                                                 |
| ----------------------- | --------- | --------------------------------------------------------------------------- |
| mcu.brd                 | board     | Main board file; for the top layer containing the MCU                       |
| mcu.sch                 | schematic | Main schematic file; for the top layer containing the MCU                   |
| motor.brd               | board     | Board file for the bottom layer containing the motor driver                 |
| motor.sch               | schematic | Schematic file for the bottom layer containing the motor driver             |
| Libraries/DRV8323RS.lbr | library   | Library file for the DRV8323RS motor driver from Texas Instruments          |

### Other Files
| Files                   | Type      | Description                                                                 |
| ----------------------- | --------- | --------------------------------------------------------------------------- |
| Renders/*.png           | image     | Collection of rendered images showing the board as it will be manufactured. |

## Authors

* **Charlie Refvem** - *Initial work* - [crefvem@calpoly.edu](mailto:crefvem@calpoly.edu)

See also the list of [members](https://bitbucket.org/blmc/profile/members) who participated in this project.

## License

*Copyright (C) 2018 Charlie Refvem* These files are not to be used or distributed by anyone without explicit consent from the author.

## Acknowledgments

* Thanks to ...
